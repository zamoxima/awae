Cheatsheet
==========

Nmap
----

### Host Discovery

```bash
nmap -sn -n 192.168.0.0/24 | grep for | cut -d" " -f5
```

### Scan

```bash
nmap -sC -sV 192.168.0.1 # Quick TCP scan
nmap -sU -sV 192.168.0.1 # Quick UDP scan
nmap -sC -sV -p- 192.168.0.1 # Full TCP scan
nmap -n -Pn -sS -A --open -p- 192.168.0.1 # No ping scan, no DNS resolution, show only open ports
```

Web Enumeration
---------------

```bash
gobuster dir -u http://192.168.0.1/ -k -w ~/wordlists/assetnote-wordlists/manual/raft-large-directories-lowercase.txt
```

Bash
----

```bash
python -c 'import pty; pty.spawn("/bin/bash")'
echo $TERM # (local)
export TERM=screen
stty raw -echo # (local)
```

John
----

```bash
unshadow passwd.txt shadow.txt > hashes.txt
john --wordlist=~/wordlists/rockyou.txt hashes.txt
```